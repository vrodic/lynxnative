package sample

import kotlinx.cinterop.*
import platform.posix.*

fun main(args: Array<String>) {
    val output = fopen("/dev/stdout", "w")

    var bufferLength = 64 * 1024

    var fileNames: Array<String>

    if (args.isEmpty()) {
        fileNames = arrayOf("/dev/stdin")
        bufferLength = 1
    } else {
        fileNames = args
    }

    for (fileName in fileNames) {
        val input = fopen(fileName, "r")

        try {
            memScoped {

                val buffer = allocArray<ByteVar>(bufferLength)

                while(true) {
                    val ret = fread(buffer, 1, bufferLength.toULong(), input)

                    if (ret.toLong() > 0) {
                        fwrite(buffer,1, ret, output)
                    } else {
                        break
                    }

                }
            }
        } finally {
            fclose(input)
        }
    }

    fclose(output)
}